
CPMAddPackage(
  NAME glm
  GITHUB_REPOSITORY g-truc/glm
  GIT_TAG 0.9.9.8
)

message(STATUS ${CMAKE_CXX_PLATFORM_ID})

if(NOT(CMAKE_CXX_PLATFORM_ID MATCHES "emscripten"))
    CPMAddPackage(
        NAME SDL2
        VERSION 2.0.14
        URL https://www.libsdl.org/release/SDL2-2.0.14.zip
        OPTIONS
            "SDL_SHARED Off"
    )

endif()
