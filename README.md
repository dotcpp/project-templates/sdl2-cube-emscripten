# sdl2-cube-emscripten

Minimal SDL2 project with the latest OpenGL, buildable wth [Emscripten](https://emscripten.org/).

This project makes use of the following libraries/projects:

* [SDL2](https://www.libsdl.org/)
* [GLM](https://github.com/g-truc/glm)
* [CPM](https://github.com/cpm-cmake/CPM.cmake)
* [GLAD](https://glad.dav1d.de/)

![Screenshot](screenshot.png)

Download emscripten on Windows and run the following commands from the emscripten folder (containing emsdk.bat and emsdk_env.bat):

```
emsdk install latest
emsdk activate latest
emsdk_env.bat
```

Build this with emscripten on Windows from the project source folder:

```
emcmake cmake -Bbuild -S.
cd build
emmake make
```
