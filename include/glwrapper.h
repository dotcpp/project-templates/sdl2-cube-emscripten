#ifndef GLWRAPPER_H
#define GLWRAPPER_H

#ifndef __EMSCRIPTEN__
#include <glad/glad.h>
#else
#define GL_GLEXT_PROTOTYPES
#include <SDL_opengl.h>
#include <SDL_opengl_glext.h>
#endif // __EMSCRIPTEN__

#endif // GLWRAPPER_H
