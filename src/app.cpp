#include <app.hpp>

#include <glwrapper.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glprogram.hpp>
#include <glshader.hpp>
#include <iostream>

void App::OnInit()
{
    glClearColor(0.56f, 0.7f, 0.67f, 1.0f);
    glEnable(GL_DEPTH_TEST);

    glGenVertexArrays(1, &_vao);
    glGenBuffers(1, &_vbo);

    glBindVertexArray(_vao);
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)0);
    glEnableVertexAttribArray(0);
    // texture coord attribute
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

#ifndef __EMSCRIPTEN__
    GlShader<GL_VERTEX_SHADER> vs(GLSL(
        layout(location = 0) in vec3 aPos;
        layout(location = 1) in vec2 aTexCoord;

        out vec3 Color;

        uniform mat4 model;
        uniform mat4 view;
        uniform mat4 projection;

        vec3 hsv2rgb(vec3 c) {
            vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
            vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
            return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
        }

        void main() {
            gl_Position = projection * view * model * vec4(aPos, 1.0f);
            Color = hsv2rgb(vec3(aTexCoord.x, aTexCoord.y, 1.0));
        }));

    GlShader<GL_FRAGMENT_SHADER> fs(GLSL(
        out vec4 FragColor;

        in vec3 Color;

        void main() {
            FragColor = vec4(Color.rgb, 1.0);
        }));

#else

    GlShader<GL_VERTEX_SHADER> vs(GLES(
        attribute vec3 aPos;
        attribute vec2 aTexCoord;

        varying vec3 Color;

        uniform mat4 model;
        uniform mat4 view;
        uniform mat4 projection;

        vec3 hsv2rgb(vec3 c) {
            vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
            vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
            return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
        }

        void main() {
            gl_Position = projection * view * model * vec4(aPos, 1.0);
            Color = hsv2rgb(vec3(aTexCoord.x, aTexCoord.y, 1.0));
        }));

    GlShader<GL_FRAGMENT_SHADER> fs(GLES(
        precision mediump float;

        varying vec3 Color;

        void main() {
            gl_FragColor = vec4(Color.rgb, 1.0);
        }));
#endif // __EMSCRIPTEN__
    _program = std::unique_ptr<GlProgram>(new GlProgram());

    _program->attach(vs);
    _program->attach(fs);
    _program->link();

    _program->use();
}

void App::OnResize(int width, int height)
{
    if (height < 1) height = 1;

    glViewport(0, 0, width, height);

    _projection = glm::perspective(glm::radians<float>(90), float(width) / float(height), 0.1f, 1000.0f);
}

void App::OnFrame()
{
    static float rotA = 0.0f, rotB = 0.0f;

    rotA += 0.01f;
    rotB += 0.024f;

    glm::quat q(glm::vec3(rotA, rotB, 0.0f));

    auto modelMatrix = glm::toMat4(q);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    _program->use();

    auto viewMatrix = glm::lookAt(
        glm::vec3(1.0f, -1.0f, 1.0f),
        glm::vec3(0.0f, 0.0f, 0.0f),
        glm::vec3(0.0f, 0.0f, 1.0f));

    unsigned int model = _program->getUniformLocation("model");
    glUniformMatrix4fv(model, 1, GL_FALSE, glm::value_ptr(modelMatrix));

    unsigned int view = _program->getUniformLocation("view");
    glUniformMatrix4fv(view, 1, GL_FALSE, glm::value_ptr(viewMatrix));

    unsigned int projection = _program->getUniformLocation("projection");
    glUniformMatrix4fv(projection, 1, GL_FALSE, glm::value_ptr(_projection));

    glBindVertexArray(_vao);
    glDrawArrays(GL_TRIANGLES, 0, 36);
}

void App::OnExit()
{
}

float App::vertices[180] = {
    -0.5f, -0.5f, -0.5f, 0.4f, 0.6f,
    0.5f, -0.5f, -0.5f, 0.4f, 0.6f,
    0.5f, 0.5f, -0.5f, 0.4f, 0.6f,
    0.5f, 0.5f, -0.5f, 0.4f, 0.6f,
    -0.5f, 0.5f, -0.5f, 0.4f, 0.6f,
    -0.5f, -0.5f, -0.5f, 0.4f, 0.6f,

    -0.5f, -0.5f, 0.5f, 0.4f, 0.6f,
    0.5f, -0.5f, 0.5f, 0.4f, 0.6f,
    0.5f, 0.5f, 0.5f, 0.4f, 0.6f,
    0.5f, 0.5f, 0.5f, 0.4f, 0.6f,
    -0.5f, 0.5f, 0.5f, 0.4f, 0.6f,
    -0.5f, -0.5f, 0.5f, 0.4f, 0.6f,

    -0.5f, 0.5f, 0.5f, 0.5f, 0.6f,
    -0.5f, 0.5f, -0.5f, 0.5f, 0.6f,
    -0.5f, -0.5f, -0.5f, 0.5f, 0.6f,
    -0.5f, -0.5f, -0.5f, 0.5f, 0.6f,
    -0.5f, -0.5f, 0.5f, 0.5f, 0.6f,
    -0.5f, 0.5f, 0.5f, 0.5f, 0.6f,

    0.5f, 0.5f, 0.5f, 0.5f, 0.6f,
    0.5f, 0.5f, -0.5f, 0.5f, 0.6f,
    0.5f, -0.5f, -0.5f, 0.5f, 0.6f,
    0.5f, -0.5f, -0.5f, 0.5f, 0.6f,
    0.5f, -0.5f, 0.5f, 0.5f, 0.6f,
    0.5f, 0.5f, 0.5f, 0.5f, 0.6f,

    -0.5f, -0.5f, -0.5f, 0.6f, 0.6f,
    0.5f, -0.5f, -0.5f, 0.6f, 0.6f,
    0.5f, -0.5f, 0.5f, 0.6f, 0.6f,
    0.5f, -0.5f, 0.5f, 0.6f, 0.6f,
    -0.5f, -0.5f, 0.5f, 0.6f, 0.6f,
    -0.5f, -0.5f, -0.5f, 0.6f, 0.6f,

    -0.5f, 0.5f, -0.5f, 0.6f, 0.6f,
    0.5f, 0.5f, -0.5f, 0.6f, 0.6f,
    0.5f, 0.5f, 0.5f, 0.6f, 0.6f,
    0.5f, 0.5f, 0.5f, 0.6f, 0.6f,
    -0.5f, 0.5f, 0.5f, 0.6f, 0.6f,
    -0.5f, 0.5f, -0.5f, 0.6f, 0.6f};
