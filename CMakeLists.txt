cmake_minimum_required(VERSION 3.12)

project(sdl2-cube-emscripten VERSION "0.1.0")

if(CMAKE_CXX_PLATFORM_ID MATCHES "emscripten")
    set(USE_FLAGS "-s USE_SDL=2")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${USE_FLAGS}")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${USE_FLAGS}")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${USE_FLAGS}")
    set(CMAKE_EXECUTABLE_SUFFIX .html)
endif()

include(cmake/CPM.cmake)
include(cmake/Dependencies.cmake)

configure_file(config.h.in config.h)

add_executable(sdl2-cube-emscripten
    include/app.hpp
    include/glprogram.hpp
    include/glshader.hpp
    include/glwrapper.h
    src/app-infra.cpp
    src/app.cpp
    src/program.cpp
)

target_compile_features(sdl2-cube-emscripten
    PRIVATE
        cxx_nullptr
)

target_include_directories(sdl2-cube-emscripten
    PRIVATE
        "include"
        "${PROJECT_BINARY_DIR}"
        "${SDL2_INCLUDE_DIRS}"
)

target_link_libraries(sdl2-cube-emscripten
    PRIVATE
        glm
        "${OPENGL_LIBRARIES}"
        "${SDL2_LIBRARIES}"
)

if(NOT(CMAKE_CXX_PLATFORM_ID MATCHES "emscripten"))

    target_sources(sdl2-cube-emscripten
        PUBLIC src/glad.c
    )

    target_link_libraries(sdl2-cube-emscripten
        PRIVATE
            SDL2-static
    )

endif ()
